package cmd

import "github.com/spf13/cobra"

var RootCmd = &cobra.Command{
	Use:   "file-retriever",
	Short: "file-retriever is a proxy for S3 assets which authorizes access",
}
