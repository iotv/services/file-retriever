package cmd

import (
	"net/http"

	"github.com/spf13/cobra"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"github.com/urfave/negroni"

	"gitlab.com/iotv/services/file-retriever/log"
	"gitlab.com/iotv/services/file-retriever/conf"
	"gitlab.com/iotv/services/file-retriever/lib"
	"github.com/gorilla/mux"
	"time"
	"gitlab.com/iotv/services/file-retriever/err"
)

var ServeCmd = &cobra.Command{
	Use:   "serve",
	Short: "run the file-retriever web server",
	Run:   serve,
}

func init() {
	RootCmd.AddCommand(ServeCmd)

	// Host
	ServeCmd.Flags().StringP("host", "H", "", "The host to serve on (default \"0.0.0.0\")")
	conf.Config.BindPFlag("host", ServeCmd.Flags().Lookup("host"))
	conf.Config.SetDefault("host", "0.0.0.0")

	// Port
	ServeCmd.Flags().StringP("port", "p", "", "The port HTTP server will run on (default \"4000\")")
	conf.Config.BindPFlag("port", ServeCmd.Flags().Lookup("port"))
	conf.Config.SetDefault("port", "4000")

}

func serve(_ *cobra.Command, _ []string) {
	host := conf.Config.GetString("host")
	port := conf.Config.GetString("port")

	mapping := conf.Config.GetStringMapString("bucketURIMappings")

	s3Client, s3Err := lib.NewS3Client()
	if s3Err != nil {
		log.Logger.Fatal(err.ImproperlyConfiguredError)
	}

	server := lib.Server{
		AuthorizationClient: lib.NewAuthorizationClient("https://api.iotv.co/auth/files", 500 * time.Millisecond),
		BucketURIMapping: mapping,
		RequireAuth: false,
		S3Client: s3Client,
	}

	router := mux.NewRouter()

	log.RouteManage(router)
	server.RouteFiles(router)
	router.NotFoundHandler = http.HandlerFunc(log.NotFoundHandler)

	n := negroni.New()
	n.Use(log.NewSentryPanicRecoveryMiddleware())
	n.Use(log.NewSentryErrorLoggingMiddleware())
	n.Use(log.NewHTTPLoggingMiddleware())
	n.Use(cors.Default())
	n.UseHandler(router)
	// TODO: remove this log
	log.Logger.WithFields(logrus.Fields{
		"host": host,
		"port": port,
	}).Info("running server...")

	log.Logger.WithFields(logrus.Fields{
		"config": conf.Config.AllSettings(),
	}).Info()

	http.ListenAndServe(host+":"+port, n)
}
