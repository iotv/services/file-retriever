package conf

import (
	"github.com/spf13/viper"
)

var Config *viper.Viper

type SentryConfig struct {
	IsEnabled bool
	DSN       string
}

func init() {
	Config = viper.New()

	// Automatically pull all configs from env for IOTV_FR_<var> prefix
	Config.SetEnvPrefix("iotv_fr")
	Config.AutomaticEnv()

	// Set config name and places to look for it
	Config.SetConfigName("file-retriever")
	Config.AddConfigPath(".")
	Config.AddConfigPath("/etc/file-retriever/")
	Config.ReadInConfig()

}

func GetSentryConfig() (SentryConfig, []string) {
	if !Config.InConfig("sentry") {
		return SentryConfig{
			IsEnabled: false,
			DSN:       "",
		},
			[]string{
				"sentry config is not defined",
			}
	}

	var ravenIsEnabled bool
	var warnings []string
	ravenDSN := Config.GetString("sentry.dsn")

	if Config.InConfig("sentry.isEnabled") {
		ravenIsEnabled = Config.GetBool("sentry.isEnabled")
	} else {
		ravenIsEnabled = true
		warnings = append(warnings, "sentry.isEnabled not defined but sentry block is, so assuming true")
	}

	if ravenIsEnabled && len(ravenDSN) == 0 {
		ravenIsEnabled = false
		warnings = append(warnings, "sentry.dsn is empty or undefined, turning sentry off")
	}

	return SentryConfig{
		IsEnabled: ravenIsEnabled,
		DSN:       ravenDSN,
	}, warnings
}
