package err

import (
	"net/http"
	"strconv"
	"strings"
	"time"
	"encoding/json"
)

const (
	AuthorizeFailedError      = "authorize failed"
	AWSConfigError            = "AWS config failed"
	ImproperlyConfiguredError = "improperly configured"
	NotImplementedError       = "not implemented"
	S3FailedError             = "S3 request failed"
)

type HTTPErrorResponse struct {
	Status  string             `json:"status"`
	Message string             `json:"message"`
	Fields  *map[string]string `json:"fields,omitempty"`
}

func CreateNotFoundResponse(method, path string) *HTTPErrorResponse {
	return &HTTPErrorResponse{
		Status:  strconv.Itoa(http.StatusNotFound),
		Message: "Not Found",
		Fields: &map[string]string{
			"method":        "could not find a sensible route for: " + method,
			"path":          "could not find a sensible route for: " + path,
			"authorization": "if this is not a public resource, you may have an invalid authorization token",
		},
	}
}

func CreateInternalServerErrorResponse(fields *map[string]string) *HTTPErrorResponse {
	return &HTTPErrorResponse{
		Status:  strconv.Itoa(http.StatusInternalServerError),
		Message: "internal server err",
		Fields:  fields,
	}
}

func CreateBadGatewayError(services ...string) *HTTPErrorResponse {
	return &HTTPErrorResponse{
		Status:  strconv.Itoa(http.StatusBadGateway),
		Message: "bad gateway",
		Fields: &map[string]string{
			"services": strings.Join(services, ","),
		},
	}
}

func CreateServiceUnavailableError(services ...string) *HTTPErrorResponse {
	return &HTTPErrorResponse{
		Status:  strconv.Itoa(http.StatusServiceUnavailable),
		Message: "service unavailable",
		Fields: &map[string]string{
			"services": strings.Join(services, ","),
		},
	}
}

func CreateGatewayTimeout(timeout time.Duration, services ...string) *HTTPErrorResponse {
	return &HTTPErrorResponse{
		Status:  strconv.Itoa(http.StatusGatewayTimeout),
		Message: "gateway timeout",
		Fields: &map[string]string{
			"services":   strings.Join(services, ","),
			"timeout_ns": strconv.FormatInt(timeout.Nanoseconds(), 10),
		},
	}
}

func Send401Response(rw http.ResponseWriter) {}

func Send403Response(rw http.ResponseWriter) {}

func Send404Response(rw http.ResponseWriter, method string, path string) {
	encoder := json.NewEncoder(rw)
	response := CreateNotFoundResponse(method, path)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusNotFound)
	encoder.Encode(response)
}

func Send405Response(rw http.ResponseWriter) {}

func Send500Response(rw http.ResponseWriter, fields *map[string]string) {
	encoder := json.NewEncoder(rw)
	response := CreateInternalServerErrorResponse(fields)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusInternalServerError)
	encoder.Encode(response)
}

func Send504Response(rw http.ResponseWriter, timeout time.Duration, services ...string) {
	encoder := json.NewEncoder(rw)
	response := CreateGatewayTimeout(timeout, services...)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusGatewayTimeout)
	encoder.Encode(response)
}
