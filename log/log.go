package log

import (
	log "github.com/sirupsen/logrus"
	"net/http"
	"github.com/getsentry/raven-go"
	"gitlab.com/iotv/services/file-retriever/conf"
)

var (
	Logger       *log.Logger
	SentryClient *raven.Client
)

type Entry struct {
	Level      *log.Level   `json:"level,omitempty,String"`
	Timestamp  string       `json:"timestamp"`
	Method     *string      `json:"method,omitempty"`
	Message    *string      `json:"message,omitempty"`
	DurationNS *int         `json:"duration_ns,omitempty"`
	Data       *interface{} `json:"data,omitempty"`
	RequestId  string       `json:"request_id"`
	SpanId     string       `json:"span_id"`
	ParentId   *string      `json:"parent_id,omitempty"`
}

type HTTPData struct {
	Type       string       `json:"type"`
	StatusCode *int         `json:"status_code,omitempty"`
	Headers    *http.Header `json:"headers,omitempty"`
	Method     string       `json:"method"`
	Resource   string       `json:"resource"`
	Protocol   string       `json:"protocol"`
}

type ConfigData map[string]string

func init() {
	Logger = log.New()
	Logger.Formatter = &log.JSONFormatter{}

	sentryConfig, sentryWarnings := conf.GetSentryConfig()
	for _, warning := range sentryWarnings {
		// TODO: fix this warning to be in line with logstash format
		Logger.Warn(warning)
	}

	var sentryErr error
	if sentryConfig.IsEnabled {
		SentryClient, sentryErr = raven.New(sentryConfig.DSN)
	}
	if sentryErr != nil {
		SentryClient = nil
	}
}
