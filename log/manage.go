package log

import (
	"github.com/gorilla/mux"
	"net/http"
	"fmt"
)

func heartbeat(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	fmt.Fprint(rw, "{\"status\": \"ok\"}")
}

func healthCheck(rw http.ResponseWriter, r *http.Request) {
	// TODO: switch this to a real healthcheck based on prometheus metrics from downstreams
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	fmt.Fprint(rw, "{\"status\": \"ok\"}")
}

func serverInfo(rw http.ResponseWriter, r *http.Request) {
	// TODO: pull in actual server info from config
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	fmt.Fprint(rw, "{\"status\": \"ok\"}")
}

func RouteManage(router *mux.Router) {
	router.HandleFunc("/manage/heartbeat", heartbeat).Methods(http.MethodGet)
	router.HandleFunc("/manage/health", healthCheck).Methods(http.MethodGet)
	router.HandleFunc("/manage/info", serverInfo).Methods(http.MethodGet)
	// TODO: handle metrics
	// TODO: handle swagger.yaml/swagger.json
	// TODO: get some middleware for JSON in here.
}
