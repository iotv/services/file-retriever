package log

import (
	"net/http"
	"github.com/urfave/negroni"
	"encoding/json"
	"gitlab.com/iotv/services/file-retriever/err"
	"fmt"
	"github.com/getsentry/raven-go"
	"github.com/juju/errors"
)

type JSONPanicFormatter struct{}
type SentryErrorLoggingMiddleware struct{}
type SentryPanicRecoveryMiddleware struct{}
type PrometheusMetricsMiddleware struct{}
type TracingMiddleware struct{}
type HTTPLoggingMiddleware struct{}

func (t *JSONPanicFormatter) FormatPanicError(rw http.ResponseWriter, r *http.Request, info *negroni.PanicInformation) {
	encoder := json.NewEncoder(rw)
	response := err.CreateInternalServerErrorResponse(nil)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusInternalServerError)
	encoder.Encode(response)
}

func (m *SentryErrorLoggingMiddleware) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	next(rw, r)
	resp := negroni.NewResponseWriter(rw)
	if resp.Status() >= 500 {
		SentryClient.CaptureError(r.Context().Value("error").(error), r.Context().Value("errorTags").(map[string]string))
	}

}

func (m *SentryPanicRecoveryMiddleware) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	defer func() {
		if recoverErr := recover(); recoverErr != nil {
			// TODO: log stacktrace
			recoverStr := fmt.Sprint(recoverErr)
			packet := raven.NewPacket(recoverStr, raven.NewException(errors.New(recoverStr), raven.GetOrNewStacktrace(recoverErr.(error), 2, 3, nil)), raven.NewHttp(r))
			SentryClient.Capture(packet, nil)
			err.Send500Response(rw, nil)
		}
	}()
	next(rw, r)
}

func (m *PrometheusMetricsMiddleware) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	next(rw, r)
}

func (m *TracingMiddleware) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	next(rw, r)
}

func (m *HTTPLoggingMiddleware) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	next(rw, r)
}

func NewPrometheusMetricsMiddleware() *PrometheusMetricsMiddleware {
	return &PrometheusMetricsMiddleware{}
}

func NewTracingMiddleware() *TracingMiddleware {
	return &TracingMiddleware{}
}

func NewHTTPLoggingMiddleware() *HTTPLoggingMiddleware {
	return &HTTPLoggingMiddleware{}
}

func NewSentryErrorLoggingMiddleware() *SentryErrorLoggingMiddleware {
	return &SentryErrorLoggingMiddleware{}
}

func NewSentryPanicRecoveryMiddleware() *SentryPanicRecoveryMiddleware {
	return &SentryPanicRecoveryMiddleware{}
}

func NotFoundHandler(rw http.ResponseWriter, r *http.Request) {
	err.Send404Response(rw, r.Method, r.URL.Path)
}
