package lib

import (
	"bufio"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/gorilla/mux"
	"gitlab.com/iotv/services/file-retriever/log"
	"github.com/juju/errors"
	"gitlab.com/iotv/services/file-retriever/err"
)

type Server struct {
	AuthorizationClient *AuthorizationClient
	BucketURIMapping    BucketURIMapping
	RequireAuth         bool
	S3Client            *S3Client
}

type BucketAndKey struct {
	Bucket string
	Key    string
}

func (s Server) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	// See if we have a bucket uri for that path
	bucketURI, mapBucketErr := s.lookupBucket(mux.Vars(r)["bucket"])
	if mapBucketErr != nil {
		err.Send404Response(rw, r.Method, r.URL.Path)
		return
	}

	var isAuthorized bool
	var fanOutSync sync.WaitGroup
	var authResponse *http.Response
	var s3Response *s3.GetObjectOutput
	var authErr, s3Err error

	if s.RequireAuth {
		fanOutSync.Add(1)
		go func() {
			defer fanOutSync.Done()
			authResponse, authErr = s.AuthorizationClient.Authorize(AuthorizationRequest{})
			if authErr == nil {
				isAuthorized, authErr = IsAuthorized(authResponse)
			}
		}()
	} else {
		isAuthorized = true
	}

	fanOutSync.Add(1)
	go func() {
		defer fanOutSync.Done()
		s3Response, s3Err = s.S3Client.GetObject(bucketURI, mux.Vars(r)["key"])
	}()

	fanOutSync.Wait()


	if authErr == nil && s3Err == nil && isAuthorized {
		defer s3Response.Body.Close()
		writeS3GetObjectHeaders(rw, s3Response)
		bufio.NewReader(s3Response.Body).WriteTo(rw)
	} else if !isAuthorized {
		err.Send404Response(rw, r.Method, r.URL.Path)
	} else if authErr != nil {
		log.Logger.Info("Auth error:", authErr)
		s.send5XXErrorResponse(rw, authErr, s3Err)
	} else {
		log.Logger.Info("Auth error:", s3Err)
		s.send5XXErrorResponse(rw, authErr, s3Err)
	}
}

func (s Server) lookupBucket(name string) (string, error) {
	bucketUri, ok := s.BucketURIMapping[name]
	if ok {
		return bucketUri, nil
	}
	return "", errors.New("bucket URI not provisioned")
}

func (s Server) send5XXErrorResponse(rw http.ResponseWriter, authErr, s3Err error) {
	// TODO: actually figure out what err to send
	err.Send500Response(rw, nil)
}

func (s Server) RouteFiles(router *mux.Router) {
	sub := router.PathPrefix("/files/").Subrouter()
	sub.NotFoundHandler = http.HandlerFunc(log.NotFoundHandler)
	sub.Handle("/{bucket}/{key:.*}", s)
	sub.Methods(http.MethodGet)
}

// This function is mostly bullshit because AWS doesn't know how to make a go sdk usable
func writeS3GetObjectHeaders(rw http.ResponseWriter, obj *s3.GetObjectOutput) {
	if obj.CacheControl != nil {
		rw.Header().Set("Cache-Control", *obj.CacheControl)
	}
	if obj.ContentEncoding != nil {
		rw.Header().Set("Content-Encoding", *obj.ContentEncoding)
	}
	if obj.ContentLanguage != nil {
		rw.Header().Set("Content-Language", *obj.ContentLanguage)
	}
	if obj.ContentLength != nil {
		rw.Header().Set("Content-Length", strconv.FormatInt(*obj.ContentLength, 10))
	}
	if obj.ContentType != nil {
		rw.Header().Set("Content-Type", *obj.ContentType)
	}
	if obj.ETag != nil {
		rw.Header().Set("ETag", *obj.ETag)
	}
	if obj.Expires != nil {
		rw.Header().Set("Expires", *obj.Expires)
	}
	if obj.LastModified != nil {
		rw.Header().Set("Last-Modified", (*obj.LastModified).Format(time.RFC3339))
	}
	rw.WriteHeader(http.StatusOK)
}
