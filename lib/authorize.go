package lib

import (
	"net/http"
	"encoding/json"
	"bytes"
	"time"

	"github.com/juju/errors"
	"gitlab.com/iotv/services/file-retriever/err"
)

type AuthorizationClient struct {
	UpstreamAuthorizeURL string
	HTTPClient           http.Client
}

type AuthorizationRequest struct {
	Token    string `json:"token"`
	UserId   string `json:"user_id"`
	Resource string `json:"resource"`
	Method   string `json:"method"`
}

type AuthorizationResponse struct {
	IsAuthorized        bool     `json:"is_authorized"`
	AuthorizationScopes []string `json:"authorization_scopes"`
}

func (c AuthorizationClient) Authorize(request AuthorizationRequest) (*http.Response, error) {
	reqBytes, jsonErr := json.Marshal(request)
	req, httpErr := http.NewRequest(http.MethodPost, c.UpstreamAuthorizeURL, bytes.NewBuffer(reqBytes))

	if jsonErr != nil || httpErr != nil {
		return nil, errors.New(err.AuthorizeFailedError)
	}

	req.Header.Set("Content-Type", "application/json")
	return c.HTTPClient.Do(req)
}

func IsAuthorized(r *http.Response) (bool, error) {
	decoder := json.NewDecoder(r.Body)
	var resp AuthorizationResponse

	err := decoder.Decode(&resp)
	if err != nil {
		return false, err
	}

	return resp.IsAuthorized, nil
}

func NewAuthorizationClient(url string, timeout time.Duration) *AuthorizationClient {
	return &AuthorizationClient{
		UpstreamAuthorizeURL: url,
		HTTPClient: http.Client{
			Timeout: timeout,
		},
	}
}
