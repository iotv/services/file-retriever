package lib

import (
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/juju/errors"
)

type BucketURIMapping map[string]string

type S3Client struct {
	S3Service *s3.S3
}

func init() {
}

func (s S3Client) GetObject(bucketURI string, key string) (*s3.GetObjectOutput, error){
	req := &s3.GetObjectInput{
		Bucket: aws.String(bucketURI),
		Key: aws.String(key),
	}
	return s.S3Service.GetObjectRequest(req).Send()
}

func NewS3Client() (*S3Client, error) {
	cfg, configErr := external.LoadDefaultAWSConfig()
	if configErr != nil {
		// TODO extract err text here
		return nil, errors.New("Cannot configure")
	}

	s3Service := s3.New(cfg)
	return &S3Client{
		S3Service: s3Service,
	}, nil
}
