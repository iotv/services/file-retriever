package main // import "gitlab.com/iotv/services/file-retriever"

import (
	"gitlab.com/iotv/services/file-retriever/cmd"
	"gitlab.com/iotv/services/file-retriever/log"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		log.Logger.Fatal("root cmd failed")
	}
}
