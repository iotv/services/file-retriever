FROM golang:alpine as build

# Enable for CGO off... this saves 0.1MB
# ENV CGO_ENABLED 0

COPY Gopkg.lock Gopkg.toml $GOPATH/src/gitlab.com/iotv/services/file-retriever/
COPY vendor $GOPATH/src/gitlab.com/iotv/services/file-retriever/vendor/
COPY file-retriever.go $GOPATH/src/gitlab.com/iotv/services/file-retriever/
COPY cmd $GOPATH/src/gitlab.com/iotv/services/file-retriever/cmd/
COPY conf $GOPATH/src/gitlab.com/iotv/services/file-retriever/conf/
COPY err $GOPATH/src/gitlab.com/iotv/services/file-retriever/err/
COPY lib $GOPATH/src/gitlab.com/iotv/services/file-retriever/lib/
COPY log $GOPATH/src/gitlab.com/iotv/services/file-retriever/log/

# Below is the CGO-disable install
# RUN go install -a -installsuffix cgo gitlab.com/iotv/services/file-retriever
RUN go install gitlab.com/iotv/services/file-retriever

# NOTE: this works with scratch, but there's no reason to. You save 4MB and lose debuggability
FROM alpine as run

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

COPY --from=build /go/bin/file-retriever /bin/
COPY file-retriever.yaml /etc/file-retriever/

ENTRYPOINT ["./bin/file-retriever"]

CMD ["serve"]
